"""
Definition of urls for shorter.
"""

from datetime import datetime
from django.urls import path, include
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.views.generic import TemplateView
from short_url import forms, views
from shorter.settings import DEBUG


urlpatterns = [
    path('accounts/register/', views.RegisterFormView.as_view(), name='register'),
    path('accounts/login/',
         LoginView.as_view
         (
             template_name='accounts/login.html',
             authentication_form=forms.BootstrapAuthenticationForm,
             extra_context=
             {
                 'title': 'Log in',
                 'year' : datetime.now().year,
             }
         ),
         name='login'),
    path('accounts/logout/', LogoutView.as_view(next_page='/'), name='logout'),
    path('admin/', admin.site.urls),
    path('', views.HomeView.as_view(), name='home'),
    path('', include('short_url.urls'), name='short_url'),   
]

if DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls))
    ] + urlpatterns
