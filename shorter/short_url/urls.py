from django.urls import path
from django.views.generic import TemplateView
from short_url import forms, views

urlpatterns = [
    path('list-template-url', views.ListTempletesView.as_view(), name='list_template_url'),
    path('get-short-url', TemplateView.as_view(template_name='short_url/get_short_url.html'), name='get_short_url'),
    path('converted-url', views.ConvertedUrlView.as_view(), name='converted_url'),
    path('converted-url/templates/<int:pk>', views.TemplateConvertedUrlView.as_view(), name='template'),
    path('<str:short_url>', views.RedirectShortUrlView.as_view(), name='redirect_short_url'),
]
