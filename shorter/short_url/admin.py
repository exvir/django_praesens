from django.contrib import admin

from short_url.models import ConvertedUrl, TemplateUrl


# Register your models here.
admin.site.register(ConvertedUrl)
admin.site.register(TemplateUrl)