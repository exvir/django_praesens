"""
Definition of models.
"""

import random
import re

from django.db import IntegrityError, models
from django.contrib.auth.models import User
# from django.core.urlresolvers import reverse
from django.urls import reverse_lazy
# Доступ к глобальным настройкам проекта (см. settings.py)
from django.conf import settings


# TODO сделать "срок удаления, срок действия ссылки"
class ConvertedUrl(models.Model):
    long_url = models.URLField(unique=True, verbose_name='Изначальный Url')
    short_url = models.URLField(unique=True, verbose_name='Короткий url')
    customer = models.ForeignKey(User, models.PROTECT)
    #TODO: Скорее всего понадобится при переносе логики создания длинного url по шалону из вью в модель
    #template_url = models.ForeignKey(TemplateUrl, models.PROTECT)
    #template_fields = JSONField()

    def _create_short_url(self):
        """
        В случае маловероятной генерации одинаковых ссылок, необходимо повторить генерацию

        TODO: можно было бы оптимизировать до одной операции -> сразу сохранять и ловить уже IntegrityException,
        при это варианте в большинстве случаев оно бы сразу писало, потому что вероятность достаточно минимальна
        """
        while True:
            try:
                short_url = ''.join(random.choice(settings.SHORT_URL_ALPHABET) for _ in range(settings.SHORT_URL_SIZE))
                ConvertedUrl.objects.get(short_url=short_url)
                continue
            except self.DoesNotExist:
                return short_url

    def save(self, *args, **kwargs):
        self.short_url = self._create_short_url()
        super().save(*args, **kwargs)

    def get_short_url_with_prefix(self):
        return settings.SHORT_URL_PREFIX + self.short_url

    def __str__(self):
        return f'URL {self.short_url} -> {self.customer.username}'

    class Meta:
        verbose_name = 'Сокращённое url'
        verbose_name_plural = 'Сокращённые url'


class TemplateUrl(models.Model):
    title = models.CharField(max_length=100, verbose_name='Название шаблона',
                             help_text='Название, которое будет видеть пользователь')
    template = models.CharField(max_length=250, verbose_name='Шаблон',
                                help_text='Введите строку-шаблон на основе которой, будет формироваться форма например')

    def __str__(self):
        return f'Шаблон {self.title}'

    def get_absolute_url(self):
        return reverse_lazy('template', kwargs={'template_url': self.slug})
        # return reverse('template', kwargs={'template_url': self.slug})

    def get_all_substitution_fields(self):
        fields = re.findall(r'\{[^}]+\}', self.template)
        for i in range(len(fields)):
            fields[i] = re.sub('[{}]', '', fields[i])
        return fields
