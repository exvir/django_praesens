from django.test import TestCase
from django.contrib.auth.models import User
from django.conf import settings
from ..models import ConvertedUrl, TemplateUrl


class ConvertedUrlTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(username='homer', password='simpson')
        cls.converted_url = ConvertedUrl.objects.create(long_url='https://ru.wikipedia.org/wiki/Модульное_тестирование', customer=cls.user)

    def test_create_model(self):
        self.assertEqual(ConvertedUrl.objects.all().count(), 1)

    def test_len_short_url(self):
        self.assertEqual(len(self.converted_url.get_short_url_with_prefix()), len(settings.SHORT_URL_PREFIX) + settings.SHORT_URL_SIZE)


class TemplateUrlTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.template_url = TemplateUrl.objects.create(title='Тестовый', template='http://test/{test}')

    def test_get_all_substitution_fields(self):
        fields = self.template_url.get_all_substitution_fields()
        for field in fields:
            self.assertEqual(field, 'test')
