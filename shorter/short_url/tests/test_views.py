"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".
"""

import django
from django.test import TestCase
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from ..forms import ConvertedUrlForm
from ..models import ConvertedUrl, TemplateUrl


class MyTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.username = 'homer'
        cls.password = 'simpson'
        cls.long_url = 'https://ru.wikipedia.org/wiki/Модульное_тестирование'
        cls.user = User.objects.create_user(username=cls.username)
        cls.user.set_password(cls.password)
        cls.user.save()
        cls.converted_url = ConvertedUrl.objects.create(long_url=cls.long_url, customer=cls.user)
        cls.template_url = TemplateUrl.objects.create(pk=1, title='Тестовый', template='http://test/{test}')

    def test_logged_in(self):
        logged_in = self.client.login(username=self.username, password=self.password)
        self.assertTrue(logged_in)


class HomeViewTest(MyTestCase):
    def test_authenticated_response(self):
        self.test_logged_in()
        response = self.client.get(reverse_lazy('home'))
        self.assertEqual(response.status_code, 200)

    def test_unauthenticated_response(self):
        response = self.client.get(reverse_lazy('home'))
        self.assertEqual(response.status_code, 302)

    def test_get_list_converted_urls(self):
        self.test_logged_in()
        response = self.client.get(reverse_lazy('home'))
        self.assertIsNotNone(response.context)
        self.assertEqual(response.context.get('converted_urls').first(), ConvertedUrl.objects.filter(customer=self.user).first())


class ListTemplatesTest(MyTestCase):
    # @classmethod
    # def setUpTestData(cls):
    #     super().setUpTestData()
    #     cls.template_url = TemplateUrl.objects.create(title='Тестовый', template='http://test/{test}')

    def test_authenticated_response(self):
        self.test_logged_in()
        response = self.client.get(reverse_lazy('list_template_url'))
        self.assertEqual(response.status_code, 200)

    def test_unauthenticated_response(self):
        response = self.client.get(reverse_lazy('list_template_url'))
        self.assertEqual(response.status_code, 302)

    def test_context(self):
        self.test_logged_in()
        response = self.client.get(reverse_lazy('list_template_url'))
        self.assertIsNotNone(response.context)
        self.assertEqual(response.context.get('templates_url').first(), self.template_url)


class TemplateConvertedUrlTest(MyTestCase):
    def test_authenticated_response(self):
        self.test_logged_in()
        response = self.client.get(reverse_lazy('template', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 200)

    def test_unauthenticated_response(self):
        response = self.client.get(reverse_lazy('template', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 302)

    def test_context_get_response(self):
        self.test_logged_in()
        response = self.client.get(reverse_lazy('template', kwargs={'pk': 1}))
        self.assertIsNotNone(response.context)
        self.assertIsNotNone(response.context['template_url'])
        self.assertIsNotNone(response.context['form'])

    def test_received_form_fields(self):
        self.test_logged_in()
        response = self.client.get(reverse_lazy('template', kwargs={'pk': 1}))
        form = response.context['form']
        for field in form.fields:
            self.assertEqual(field, 'test')

    def test_response_code_after_post_request(self):
        self.test_logged_in()
        response = self.client.post(reverse_lazy('template', kwargs={'pk': 1}), data={
            'test': 'test-value',
        })
        self.assertEqual(response.status_code, 200)

    def test_context_after_post_request(self):
        self.test_logged_in()
        response = self.client.post(reverse_lazy('template', kwargs={'pk': 1}), data={
            'test': 'test-value',
        })
        response.context['short_url']
        item = ConvertedUrl.objects.get(long_url='http://test/test-value')
        self.assertIsNotNone(response.context)
        self.assertEqual(response.context.get('short_url'), item.short_url)


class ConvertedUrlTest(MyTestCase):
    def test_authenticated_response(self):
        self.test_logged_in()
        response = self.client.get(reverse_lazy('converted_url'))
        self.assertEqual(response.status_code, 200)

    def test_unauthenticated_response(self):
        response = self.client.get(reverse_lazy('converted_url'))
        self.assertEqual(response.status_code, 302)

    def test_post_response_code(self):
        self.test_logged_in()
        response = self.client.post(reverse_lazy('converted_url'), data={
            'long_url': self.long_url,
        })
        self.assertEqual(response.status_code, 200)

    def test_created_short_url_after_post_request(self):
        self.test_logged_in()
        response = self.client.post(reverse_lazy('converted_url'), data={
            'long_url': self.long_url,
        })
        item = ConvertedUrl.objects.get(long_url=self.long_url)
        self.assertIsNotNone(item)

    # def test_context_of_response(self):
    #     self.test_logged_in()
    #     response = self.client.post(reverse_lazy('converted_url'), data={
    #         'long_url': self.long_url,
    #     })
    #     item = ConvertedUrl.objects.get(long_url=self.long_url)
    #     self.assertIsNotNone(response.context)
    #     self.assertEqual(response.context.get('short_url'), item.short_url)


class RedirectShortUrlTest(MyTestCase):
    def test_request_code(self):
        self.response = self.client.get(self.converted_url.get_short_url_with_prefix())
        self.assertEqual(self.response.status_code, 302)
