"""
Definition of forms.
"""
import re

from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _

from short_url.models import ConvertedUrl


class BootstrapAuthenticationForm(AuthenticationForm):
    """Authentication form which uses boostrap CSS."""
    username = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'class': 'form-control',
                                   'placeholder': 'User name'}))
    password = forms.CharField(label=_("Password"),
                               widget=forms.PasswordInput({
                                   'class': 'form-control',
                                   'placeholder': 'Password'}))


class BootstrapUserCreationForm(UserCreationForm):
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput({
                                   'class': 'form-control',
                                   'placeholder': 'Пароль'}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput({
                                   'class': 'form-control',
                                   'placeholder': 'Повторите ваш пароль'}),
        strip=False,
        help_text=_("Введите тот же пароль, что и раньше, для проверки."),
    )
    class Meta:
        model = User
        fields = ("username",)
        widgets = {
            "username": forms.TextInput({
                                   'class': 'form-control',
                                   'placeholder': 'Имя пользователя'}),
        }


class ConvertedUrlForm(forms.ModelForm):
    class Meta:
        model = ConvertedUrl
        fields = ['long_url']
        labels = {
            "long_url": "Ваша ссылка",
        }


class TemplateForm(forms.Form):
    def __init__(self, *args, **kwargs):
        fields = kwargs.pop('fields')
        super().__init__(*args, **kwargs)
        for x in fields:
            self.fields[x] = forms.CharField(max_length=50)