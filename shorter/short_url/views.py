"""
Definition of views.
"""

from datetime import datetime
import re

from django.shortcuts import redirect, render, render_to_response, get_object_or_404
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import FormView
from django.views.generic import View, TemplateView, ListView, UpdateView, DetailView
from django.views.generic.detail import DetailView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.sites.shortcuts import get_current_site

from .models import ConvertedUrl, TemplateUrl
from .forms import ConvertedUrlForm, TemplateForm, BootstrapUserCreationForm

#TODO: Во всём модуле указан путь перенаправления для логгирования, нужно проверить можно ли его вынести в настройки и не указывать явно
class RegisterFormView(FormView):
    form_class = BootstrapUserCreationForm
    success_url = reverse_lazy('home')
    template_name = 'accounts/register.html'

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)


class HomeView(LoginRequiredMixin, ListView):
    template_name = 'main/home.html'
    login_url = reverse_lazy('login')
    context_object_name = 'converted_urls'

    def get_queryset(self):
        self.user = self.request.user
        return ConvertedUrl.objects.filter(customer=self.user)
        

class ListTempletesView(LoginRequiredMixin, ListView):
    template_name = 'short_url/list_templates_url.html'
    login_url = reverse_lazy('login')
    context_object_name = 'templates_url'
    
    def get_queryset(self):
        return TemplateUrl.objects.all()


class TemplateConvertedUrlView(LoginRequiredMixin, DetailView, UpdateView):
    login_url = reverse_lazy('login')
    form_class = TemplateForm
    template_name = 'short_url/template_form_converted_url.html'

    def get(self, request, *args, **kwargs):
        template_url = get_object_or_404(TemplateUrl, pk=kwargs['pk'])
        fields = template_url.get_all_substitution_fields()
        form = TemplateForm(fields=fields)
        return render(request, self.template_name, {'form': form, 'template_url': template_url})

    def post(self, request, *args, **kwargs):
        template_url = get_object_or_404(TemplateUrl, pk=kwargs['pk'])
        fields = template_url.get_all_substitution_fields()
        form = TemplateForm(request.POST, fields=fields)
        if form.is_valid():
            data = request.POST
            template = template_url.template
            template = template.format(**data)
            template = re.sub('[\[\]\'\']', '', template)
            user = request.user
            converter_url = ConvertedUrl.objects.create(long_url=template, customer=user)
            short_url = converter_url.short_url
            return render(request, 'short_url/get_short_url.html', {'short_url': short_url})
        return render(request, self.template_name, {'form': form, 'template_url': template_url})


#TODO: пока не уверен что нужно эту вью сохранять
class ConvertedUrlView(LoginRequiredMixin, FormView):
    login_url = reverse_lazy('login')
    form_class = ConvertedUrlForm
    template_name = 'short_url/converted_url_form.html'
    
    def form_valid(self, form):
        long_url = form.cleaned_data['long_url']
        user = self.request.user
        converter_url = ConvertedUrl.objects.create(long_url=long_url, customer=user)
        short_url = converter_url.short_url
        return render_to_response('short_url/home.html')


class RedirectShortUrlView(View):

    def get(self, request, **kwargs):
        short_url = self.kwargs['short_url']
        self.converted_url = get_object_or_404(ConvertedUrl, short_url=short_url)
        return redirect(self.converted_url.long_url)